//deklaracja zmiennych globalnych
var usernamegl
var roomcurrent = 0
var socket = io.connect('http://localhost:3200')
	
	$( document ).ready(function() {
		var cookies = Cookies.get()
		$.ajax({ 
		   type : "GET", 
		   url : "/api/users/current", 
		   beforeSend: function(xhr){xhr.setRequestHeader('Authorization', cookies.Authorization);},
		   success : function(result) { 

		   }, 
		   error : function(result) { 
		     window.location.href = "/"
		   } 
		 }); 
		usernamegl = cookies.Nickname
		socket.emit('send_username', {username: usernamegl})
		socket.emit('userlist_get')
	});

$(function(){

	var message = $('#message')
	var send_message = $('#send_message')
	var send_username = $('#send_username')
	var chat = $('#chat')
	var feedback = $('#feedback')
	var userlist = $('#userlist')
	var testroom = $('#testroom')

	chat.scrollTop(1000000);

	send_message.click(function(){
		socket.emit('new_message', {room: roomcurrent, user: usernamegl, message: message.val()})
		message.val("")
		socket.emit('typed')
		refreshCookies()
	})
	message.keyup(function(){
		if (event.keyCode === 13) {
			event.preventDefault();
			send_message.click()
		}
	})
	socket.on('userlist_receive', (data)=>{
		userlist.empty()
		users = [...data.usernamelist]
		usernamecur = usernamegl
		if (users.find(checkusrex) == usernamecur) {
			var checkusr = users.indexOf(usernamecur)
			users.splice(checkusr, 1)
		}
		userslenght=users.length
		if (userslenght>0) {
			for (var i = 0; i < userslenght; i++) {
				userlist.append("<li ><a class='user' id="+users[i]+" onclick='selectreceiver(id)' href='#"+users[i]+"'>"+users[i]+"</a></p>")
			}	
		}
	})

	socket.on('userlist_refresh', ()=>{
		socket.emit('userlist_get')
	})

	socket.on('new_message', (data)=>{
		chat.append("<p class='message'>" + data.username+ ": " + data.message + "</p>")
		var chatDiv = document.getElementById("chat")
		chatDiv.scrollIntoView(false);
	})

	socket.on('getPastMessages', (data)=>{
		chat.html("")
		for (var i = 0; i < data.logs.length; i++) {
			chat.append("<p class='message'>" + data.logs[i].user+ ": " + data.logs[i].message + "</p>")
		}
		var chatDiv = document.getElementById("chat")
		chatDiv.scrollIntoView(false);
	})

	message.bind("keypress", ()=>{
		socket.emit('typing')
	})

	socket.on('typing', (data)=>{
		feedback.html("<p><i>" + data.username + " is typing a message..." + "</i></p>")
	})	

	socket.on('typed', ()=>{
		feedback.html("")
	})
	socket.on('connectToRoom', (data)=>{
		console.log(data.room)
		roomcurrent = data.room
	})

})

function checkusrex(usr) {
	usernamecur = usernamegl
  	return usr == usernamecur

  	refreshCookies();
}

function selectreceiver(target){
	socket.emit('change_room', {nickname1: usernamegl, nickname2: target})
	refreshCookies();
}

function test(){
	socket.emit('test')
}
function logout(){
		console.log('Logout')
		Cookies.remove("Nickname")
		Cookies.remove("Authorization")
		socket.emit('disconnect')
		setTimeout(function(){
				window.location.href = "/"
		}, 5)
}

function searchuser() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('searchuser');
  filter = input.value.toUpperCase();
  ul = document.getElementById("userlist");
  li = ul.getElementsByTagName('li');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName("a")[0];
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}


function refreshCookies(){
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	var d = new Date();
	d.setTime(d.getTime() + (1 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
    for (var i = 0; i < ca.length; i++) {
		document.cookie = ca[i] + ";" + expires + ";path=/";
  	}
}


