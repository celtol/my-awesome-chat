var socket_fu = io.connect('http://localhost:3400')
var uploader = new SocketIOFileUpload(socket_fu);
var delivery = new Delivery(socket_fu);
$(function(){
	var fileListModalList = $('#fileList')

	socket_fu.on('fileList', (data)=>{
		console.log(data.filelist)
		fileListModalList.html("")
		for (var i = 0; i < data.filelist.length; i++) {
			fileListModalList.append("<li class='fileItem'><button class='filelink' id='"+data.filelist[i]+"' onclick='getfile(id)'>"+data.filelist[i]+"</button></li>")
		}
	})

	socket_fu.on('sendLink', (data)=>{
		window.open(data.link, "_self")
	})

})
function sendfile(event){
	var room = roomcurrent
	socket_fu.emit('send_room', {room: roomcurrent})
	files = event.target.files
	uploader.submitFiles(files)
}

function openFileListModal(){
	var room = roomcurrent
	$('#filelistmodal').modal('show')
	socket_fu.emit('getRoomFileList', {room: room})
}

function getfile(data){
	socket_fu.emit('getFile', {room: roomcurrent, file: data})
}