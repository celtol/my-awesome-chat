const express = require('express');
const ip = require('ip');
//inicjalizacja modułów odpowiedzialnych za logowanie
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const mongoose = require('mongoose');
const errorHandler = require('errorhandler');
//inicjallizacja modułu odpowiedzialnego za zarządzanie systemem plików
const path = require('path');
const fs = require('fs');
//inicjalizacja modułu odpowiedzialnego za przesyłanie wiadomości
const chat = express()
  .listen(3200, () => console.log('Chat socket: 3200'));
const io_chat = require('socket.io')(chat);
//inicjalizacja modułu odpowiedzialnego za przesyłanie plików
const io_fu = require('socket.io');
const siofu = require("socketio-file-upload");
const dl = require('delivery');
const _ = require('underscore');
const fu = express()
  .use(siofu.router)
  .use(express.static(__dirname + "/uploads"))
  .listen(3400, () => console.log('File upload socket: 3400'));


//konfiguracja bazy danych
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

//inicjalizacja aplikacji
const login = express();

//konfiguracja aplikacji
login.use(cors());
login.use(require('morgan')('dev'));
login.use(bodyParser.urlencoded({ extended: false }));
login.use(bodyParser.json());
//zdeklarowanie gdzie strony mają szukać zasobów
login.use(express.static(path.join(__dirname, 'public')));
//deklaracja silnika widoku
login.set('view engine', 'ejs');
//deklaracja strony znajdującej się pod adresem localhost/
login.get('/', (req, res) => {
  res.render('login')
});
//deklaracja strony znajdującej się pod adresem localhost/register
login.get('/register', (req, res) => {
  res.render('register')
});
//deklaracja strony znajdującej się pod adresem localhost/chat
login.get('/chat', (req, res) => {
  res.render('chat')
})
//deklaracja odnoścników do pobierania mediów
login.get('/uploads/:room/:file', (req,res)=>{
  var room = req.params.room;
  var filename = req.params.file;
  res.download(__dirname + '/uploads/'+room+'/'+filename, filename)
})
//deklaracja cookie sesji
login.use(session({ secret: 'login-info', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));
//deklaracja używanego portu do hostingu stron
login.listen(80, () => console.log('Login socket: 80'));



//konfiguracja modułu mongoose do komunikacji z bazą danych MongoDB
mongoose.connect('mongodb://localhost/chat-aplication');
//konfiguracja trybu debugowania
mongoose.set('debug', false);

//modele i routing
require('./models/Users');
require('./models/Rooms');
require('./models/Files')
require('./config/passport');
login.use(require('./routes'));
const Rooms = mongoose.model('Rooms');
const Files = mongoose.model('Files');

//lista aktualnie zalogowanych użytkowników
var users_online = [];

//ustawienia i logika chatu
io_chat.on('connection', function(socket) {
  //wydarzenie send_username wykonujące doddanie nazwy użytkownika do danych socket oraz do listy aktywnych użytkowników
  socket.on('send_username', (data)=>{
    users_online.push(data.username)
    socket.username = data.username
    temp1 = socket.rooms
    for (var i = 0; i < temp1.length; i++) {
      socket.leave(temp1[i]);
    }
    console.log('New user connected: ' + data.username)
    console.log('Avaible users: ' + users_online)
  })
  //wydarzenie test pokazujące różne dane
  /*socket.on('test', ()=>{
    console.log(socket)
    console.log(socket.username)
    console.log(socket.foundroom)
  })*/
  //wydarzenie change_room wykonujące dołączanie użytkownika do wybranej przez niego rozmowy
  socket.on('change_room', (data) => {
    socket.rooms = {}

    //deklaracje zapytań do bazy danych
    var roomquery1 = Rooms.find({ nickname1: data.nickname1, nickname2: data.nickname2 })
    var roomquery2 = Rooms.find({ nickname1: data.nickname2, nickname2: data.nickname1 })
    //wykonanie zapytań i przekazanie jej wyników do obiektu promise
    var promise1 = roomquery1.exec()
    var promise2 = roomquery2.exec()
    //przetworzenie danych z obiektu po wykonaniu zapytania
    promise1.then(function(doc1){
      //sprawdzenie czy istanieje pokój/rozmowa zdeklarowanych użytkowników
      if (doc1.length != 0) {
        //jeżeli w zapytaniu zneleziono rozmowę to użytkownik zostaje do niej przekierowany
        socket.foundroom = doc1[0].room
        socket.join(doc1[0].room)
        socket.emit('connectToRoom', {room: doc1[0].room});
        //wczytanie i wysłanie użytkownikowi zapisu rozmów z ostatniego dnia rozmów
        var getlog_date
        directory = "./logs/" + doc1[0].room + "/"
        var log_filename = getMostRecentFileName(directory)
        var log_content = fs.readFileSync(directory+log_filename).toString().split(",\n")
        var log_array = []
        for (var i = 0; i < log_content.length - 1; i++) {
          var temp_json = JSON.parse(log_content[i]);
          log_array.push(temp_json)
        }
        socket.emit('getPastMessages', {logs: log_array})
      }else if (doc1.length == 0) {
        //sprawdzenie kolejnego zapytania jeżeli pierwsza wersja nie przyniosła wyniku
        promise2.then(function(doc2){
          if (doc2.length != 0) {
            //jeżeli w zapytaniu zneleziono rozmowę to użytkownik zostaje do niej przekierowany
            socket.foundroom = doc2[0].room
            socket.join(doc2[0].room)
            socket.emit('connectToRoom', {room: doc2[0].room});
            //wczytanie i wysłanie użytkownikowi zapisu rozmów z ostatniego dnia rozmów
            var getlog_date
            directory = "./logs/" + doc2[0].room + "/"
            var log_filename = getMostRecentFileName(directory)
            var log_content = fs.readFileSync(directory+log_filename).toString().split(",\n")
            var log_array = []
            for (var i = 0; i < log_content.length - 1; i++) {
              var temp_json = JSON.parse(log_content[i]);
              log_array.push(temp_json)
            }
            socket.emit('getPastMessages', {logs: log_array})
          }else if (doc2.length == 0) {
            //jeżeli żadne z zapytań nie zwróciło wyniku to zostaje stworzony nowy pokój/rozmowa i użytkownik zostaje do niej podłącznony
            //tworzenie nowego numeru pokoju opiera się stworzeniu inkrementacji w oparciu o aktualną liczbę rozmów/pokoi
            Rooms.countDocuments({}, function( err, count){
                var newroom = {}
                //tworzenie obiektu w celu utworzenia nowego rekordu/dokumentu w bazie danych
                newroom.nickname1 = data.nickname1
                newroom.nickname2 = data.nickname2
                newroom.room = count + 1
                newroom.last_contact = new Date()

                setTimeout(function(){
                  //tworzenie nowego dokumentu dla modelu Rooms w bazie danych MongoDB
                  const finalNewroom = new Rooms(newroom)
                  finalNewroom.save()

                }, 5)
                //połączenie użytkownika z rozmową/pokojem
                socket.foundroom = newroom.room
                socket.join(newroom.room)
                socket.emit('connectToRoom', {room: newroom.room});
            })
            
          }
        })
      }
    })
  })
  //wydarzenie new_message powoduje zapisanie wiadomości wysłanej do pokoju do logów dnia
  socket.on('new_message', (data)=>{
    //zapisanie danych do logów
    var temp_date = new Date()
    var log_date = temp_date.getMonth() + "_" + temp_date.getDate() + "_" + temp_date.getFullYear()
    directory = "./logs/" + data.room + "/"
    messageToObj = {
      user: data.user,
      message: data.message
    }
    messageToString = JSON.stringify(messageToObj);
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
    setTimeout(function(){

    fs.appendFile(directory + log_date + ".txt", messageToString +","+ "\n", function(err) {
      if(err) {
          return console.log(err);
      } 
    }); 
    },5)
    //wysłanie wiadomości do pokoju
    io_chat.sockets.in(data.room).emit('new_message', {username: data.user, message: data.message})
  })
  //wydarzenie userlist_get wysyła listę aktualnie zalogowanych użytkowników
  socket.on('userlist_get', ()=>{
    io_chat.sockets.emit('userlist_receive', {usernamelist: users_online})
  })

  //wydarzenie disconnect uaktulania listę aktualnie zalogowanych użytkowników po wylogowaniu lub zamknięciu połączenia
  socket.on('disconnect', ()=>{
    temp1 = users_online.indexOf(socket.username)
    users_online.splice(temp1, 1)
    io_chat.sockets.emit('userlist_refresh')
  })
})



//konfiguracja wysyłania plików
var io_filup = io_fu.listen(fu);
io_filup.sockets.on("connection", function(socket) {

  //definiowanie podstawowych ustawień
  var uploader = new siofu();
  uploader.listen(socket);
  uploader.dir = "./uploads/tmp";
  var delivery = dl.listen(socket);

  //wydarzenie send_room odpowiada za przetworzenie wysłanego pliku do serwera i zapisanie go w odpowienim folderze rozmowy/pokoju
  socket.on('send_room', (data) => {
    dir = "./uploads/" + data.room
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    uploader.dir = "./uploads/" + data.room;
    //wydarzenie saved tworzy wpis w bazie danych dotyczący wysłanego do serwera pliku
    uploader.on("saved", function(event) {
      var file = {}
      file.file_name = event.file.name
      file.room = data.room
      var date = new Date()
      file.upload_date = date

      setTimeout(function(){
        const finalFile = new Files(file);
        finalFile.save()

      }, 5)
    });
  })

  //wydarzenie error wyświatle treść błędu w konsoli serwera
  uploader.on("error", function(event) {
    console.log("Error from uploader", event);
  });
  //wydarzenie getRoomFileList odpowiada za wysłanie do użytkowanika aktualnej listy plików wysłanej do danej rozmowy/pokoju
  socket.on("getRoomFileList", (data)=>{
    var filelist=[]
    filelist.length = 0;
    directoryPath = "./uploads/"+ data.room
    fs.readdir(directoryPath, function (err, files) {
    //handling error
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    } 
    files.forEach(function (file) {
        filelist.push(file) 
    });
    socket.emit('fileList', {filelist: filelist})
  });
  //wydarzenie getFile odpowiada za wysłanie do użytkownika linku do pobrania pliku
  socket.on('getFile', (data)=>{
    socket.emit('sendLink',{link: "http://"+ip.address()+"/uploads/"+data.room+"/"+data.file})
  })

  })

});

function getMostRecentFileName(dir) {
    var files = fs.readdirSync(dir);
    // use underscore for max()
    return _.max(files, function (f) {
        var fullpath = path.join(dir, f);
        // ctime = creation time is used
        // replace with mtime for modification time
        return fs.statSync(fullpath).mtime;
    });
}