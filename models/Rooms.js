const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const RoomsSchema = new Schema({
  nickname1: String,
  nickname2: String,
  room: String,
  last_contact: String
});

mongoose.model('Rooms', RoomsSchema);