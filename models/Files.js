const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const FilesSchema = new Schema({
  file_name: String,
  room: String,
  upload_date: String
});

mongoose.model('Files', FilesSchema);